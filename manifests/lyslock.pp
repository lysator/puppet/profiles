class profiles::lyslock {
firewall { '100 Disallow zwaveUI external connections':
    proto    => tcp,
    dport    => '8091',
    jump     => drop,
    protocol => 'iptables'
  }

firewall { '101 Allow port 80 for acmetool':
    proto    => tcp,
    dport    => '80',
    jump     => accept,
    protocol => 'iptables'
  }

}
