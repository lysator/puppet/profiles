class profiles::repomaster::rpm {
  class { '::lyslogin':
    roots_only   => true,
    has_homedirs => false,
  }

  ensure_packages(['yum-utils'],
    { ensure => installed })

  file { '/var/repos/lysator/CentOS/7/x86_64':
    ensure => directory,
  }

  require ::nginx

  nginx::resource::server { $facts['networking']['fqdn']:
    server_name      => '_',
    http2            => 'on',
    ipv6_enable      => true,
    www_root         => '/var/repos',
    ssl              => true,
    ssl_cert         => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem",
    ssl_key          => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/privkey.pem",
    ssl_trusted_cert => '/etc/letsencrypt/live/studieinfo.lysator.liu.se/fullchain.pem',
  }
}
