# Inhysning för Föreningen för Föreningsaktivas
class profiles::for3 {

  $certname = $::fqdn

  ensure_packages ([
    'vim',
  ], {
    ensure => installed,
  })

  $wwwroot = '/var/www'

  class { '::nginx':
    gzip         => 'on',
    server_purge => true,
  }

  class { '::letsencrypt':
    config  => {
      email => 'ledning@for3.lysator.liu.se',
    }
  }

  class { '::profiles::letsencrypt':
    certname => $certname,
    provider => nginx,
    domains  => [
      $::fqdn,
      'xn--fr3-sna.lysator.liu.se', # punycode('för3')
      "calendar.${::fqdn}",
      'calendar.xn--fr3-sna.lysator.liu.se', # punycode('för3')
    ]
  }

  $cert_path = $facts['letsencrypt_directory'][$certname]
  $cert_data = if $cert_path {
    {
      ssl          => true,
      ssl_redirect => true,
      ssl_cert => "${cert_path}/fullchain.pem",
      ssl_key  => "${cert_path}/privkey.pem",
    }
  } else {
    {
      ssl => false,
    }
  }

  nginx::resource::server { 'for3.lysator.liu.se':
    ensure               => present,
    server_name          => [ $::fqdn ],
    ipv6_enable          => true,
    use_default_location => false,
    ipv6_listen_options  => '',

    access_log => '/var/log/nginx/access.log',
    error_log  => '/var/log/nginx/error.log',
    # format_log => '',

    www_root    => $wwwroot,
    index_files => [ 'index.html' ],
    autoindex   => 'on',
    try_files   => [ '$uri', '$uri/', '=404', ],

    server_cfg_append => {
      # Send autoindex with explicit charset.
      charset => 'UTF-8',
    },

    *           => $cert_data,
  }


  file { '/etc/motd':
    content => "Ändringar till webbsidan görs nu på\nhttps://git.lysator.liu.se/for3/website-content\n",
  }

  nginx::resource::location {
  default:
    server         => [ 'for3.lysator.liu.se', ],
    ssl            => true,
    ssl_only       => true,
    ;
  '/':
    location_alias => '/var/www/webpage/',
    ;
  '/logobyggare':
    location_alias => '/var/www/logobyggare/',
    ;
  }

  nginx::resource::location {
    default:
      server   => [ 'calendar.for3.lysator.liu.se', ],
      ssl      => true,
      ssl_only => true,
      ;
  'Calendar /':
    location         => '/',
    proxy            => 'http://localhost:58080/',
    # proxy_redirect => '/ /calendar/',
    ;
  '/static':
    location_alias => '/usr/share/calp/www',
    autoindex      => 'on',
    ;
  }

  nginx::resource::server { 'calendar.for3.lysator.liu.se':
    ensure               => present,
    server_name          => [ "calendar.${::fqdn}", ],
    ipv6_enable          => true,
    use_default_location => false,
    ipv6_listen_options  => '',
    access_log           => '/var/log/nginx/access-calendar.log',
    error_log            => '/var/log/nginx/error-calendar.log',
    *                    => $cert_data,
  }


  # Copied from Debian 11's default config
  $smtpd_args = [
    '-o smtpd_tls_wrappermode=yes',
    '-o smtpd_sasl_auth_enable=yes',
    '-o smtpd_client_restrictions=permit_sasl_authenticated,reject',
  ].join(' ')

  class { '::postfix':
    myorigin       => 'for3.lysator.liu.se',
    relayhost      => '',
    inet_protocols => 'all',
    smtp_listen    => 'all',
    master_smtps   => [
      'smtps',  # service
      'inet',   # type
      'n',      # private
      '-',      # unpriv
      'n',      # chroot
      '-',      # wakeup
      '-',      # makeproc
      "smtpd ${smtpd_args}"   # command
      ].join("\t"),
  }

  postfix::config {
    'home_mailbox':
      value => 'Maildir/', ;
  }

  ensure_packages([
    'dovecot-imapd',
  ])

  file_line {
    default:
      path   => '/etc/dovecot/conf.d/10-mail.conf',
      notify => Service['dovecot'], ;
    'Dovecot maildir':
      match => '^#?mail_location =',
      line  => 'mail_location = maildir:~/Maildir', ;
    'Dovecot mail access group':
      match => '^#?mail_access_groups =',
      line  => 'mail_access_groups = mail', ;
  }

  file_line {
    default:
      path   => '/etc/dovecot/conf.d/10-ssl.conf',
      notify => Service['dovecot'], ;
    'cert':
      match => '^#?ssl_cert =',
      line  => "ssl_cert = <${cert_path}/fullchain.pem", ;
    'key':
      match => '^#?ssl_key =',
      line  => "ssl_key = <${cert_path}/privkey.pem", ;
  }

  file_line {
    default:
      path   => '/etc/dovecot/conf.d/10-auth.conf',
      notify => Service['dovecot'], ;
    'auth_mechanisms':
      match => '#?auth_mechanisms =',
      line  => 'auth_mechanisms = plain login',
  }
  #
  # TODO
  # i /etc/dovecot/conf.d/10-mail.conf
  # innuti `service auth {` ... `}`
  # lägg till
  $dovecot_auth = @(EOF)
  unix_listener /var/spool/postfix/private/auth {
    mode = 0666
    user = postfix
    group = postfix
  }
  | EOF

  service { 'dovecot':
    ensure => running,
    enable => true,
  }

  postfix::config {
    'smtpd_tls_cert_file':
      value => "${cert_path}/fullchain.pem", ;
    'smtpd_tls_key_file':
      value => "${cert_path}/privkey.pem", ;
    'smtpd_sasl_auth_enable':
      value => 'yes', ;
    'smtpd_sasl_type':
      value => 'dovecot', ;
    'smtpd_sasl_path':
      value => 'private/auth', ;
  }

  $mail_map = {
      "ledning@${::fqdn}"   => [
        "ordforande@${::fqdn}",
        'hugo@lysator.liu.se',
      ].join(' '),
      "ordforande@${::fqdn}" => 'foreningsaktivas.lkpg@gmail.com',
      "ordf@${::fqdn}"       => "ordforande@${::fqdn}",
      "ledningen@${::fqdn}"  => "ledning@${::fqdn}",
      "styrelse@${::fqdn}"   => "ledning@${::fqdn}",
      "styrelsen@${::fqdn}"  => "ledning@${::fqdn}",
      "styret@${::fqdn}"     => "ledning@${::fqdn}",
      "postmaster@${::fqdn}" => 'hugo@lysator.liu.se',
      'for3'                 => 'ledning',
    }

  # This updates the file, and then runs `postmap $file`, which
  # updates `$file`.db
  $map_content = $mail_map.map |$key, $value| { "${key} ${value}" }.join("\n")
  # postfix::hash { '/etc/postfix/virtual':
  #   ensure  => present,
  #   content => "${map_content}\n"
  # }

  # on mail.lysator.liu.se, /etc/postfix/relay/for3_recipients

  # virtual_alias_domains = for3.lysator.liu.se

  postfix::config { 'virtual_alias_maps':
    ensure => present,
    value  => 'hash:/etc/postfix/virtual',
  }

  class { 'profiles::locales':
    locales => [
      [ 'en_US.UTF-8', 'UTF-8' ],
      [ 'sv_SE.UTF-8', 'UTF-8' ],
      [ 'sv_SE', 'ISO-8859-1' ],
    ],
  }
}
