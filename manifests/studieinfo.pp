# Studieinfo webserver
class profiles::studieinfo {
  include ::studieinfo
  include ::lysnetwork::iptables_default_deny
  firewall { '100 accept http(s)':
    proto  => 'tcp',
    jump   => 'accept',
    dport  => ['80','443'],
  }
  firewall { '100 accept http(s) ipv6':
    proto    => 'tcp',
    jump     => 'accept',
    dport    => ['80','443'],
    protocol => 'ip6tables',
  }
}
