# Configures available locales.
# Currently only adds, and doesn't substract.
# TODO Allow setting default locale.
class profiles::locales (
  $locales = [
    [ 'en_US.UTF-8', 'UTF-8' ],
    [ 'sv_SE.UTF-8', 'UTF-8' ],
    [ 'sv_SE', 'ISO-8859-1' ],
  ]
) {

  $locales.map |$x| { $x.join(' ') }.each |$line| {
    file_line { "Locale line ${line}":
      path   => '/etc/locale.gen',
      match  => "^(# *)?${line}",
      line   => $line,
      notify => Exec['rebuild locales'],
    }
  }

  exec { 'rebuild locales':
    command     => '/usr/sbin/locale-gen',
    refreshonly => true,
  }
}
