# debian specific fixes
class profiles::base::debian {
  ensure_packages('debsecan', { ensure => absent })

  # remove the sysstat cron job, we have prometheus for monitoring
  file { '/etc/cron.d/sysstat':
    ensure => absent,
  }

  ensure_packages([
    'iptables', # To facilitate other firewall rules
    'gnupg',    # Sometimes required by apt
  ])

}
