# Inhysning för Västgöta Nation
# This profile is really similar to profiles::for3, and they should be
# merged
class profiles::vastgota {
  $certname = $::fqdn
  $servername = $::fqdn

  ensure_packages ([
    'vim',
  ], {
    ensure => installed,
  })

  $wwwroot = '/usr/share/wordpress'

  class { '::nginx':
    gzip         => 'on',
    server_purge => true,
  }

  class { '::letsencrypt':
    config  => {
      email => 'webmaster@vastgota.nation.liu.se',
    }
  }

  class { '::profiles::letsencrypt':
    certname => $certname,
    provider => nginx,
    domains  => [
      $::fqdn,
      'vastgota.nation.liu.se',
    ]
  }

  $cert_path = $facts['letsencrypt_directory'][$certname]
  $cert_data = if $cert_path {
    {
      ssl          => true,
      ssl_redirect => true,
      ssl_cert => "${cert_path}/fullchain.pem",
      ssl_key  => "${cert_path}/privkey.pem",
    }
  } else {
    {
      ssl => false,
    }
  }

  nginx::resource::server { $servername:
    ensure               => present,
    server_name          => [
      $::fqdn,
      'vastgota.nation.liu.se',
    ],
    ipv6_enable          => true,
    use_default_location => false,
    ipv6_listen_options  => '',
    access_log           => '/var/log/nginx/access.log',
    error_log            => '/var/log/nginx/error.log',
    www_root             => $wwwroot,
    index_files          => [ 'index.html' ],
    autoindex            => 'on',
    try_files            => [ '$uri', '$uri/', '=404', ],
    *                    => $cert_data,
  }

  ##################################################

  # Copied from Debian 11's default config
  $smtpd_args = [
    '-o smtpd_tls_wrappermode=yes',
    '-o smtpd_sasl_auth_enable=yes',
    '-o smtpd_client_restrictions=permit_sasl_authenticated,reject',
  ].join(' ')

  class { '::postfix':
    myorigin       => 'vastgota.nation.liu.se',
    mydestination  => [
      'vastgota.nation.liu.se',
      $::fqdn,
      'localhost.$mydomain',
      'localhost.nation.liu.se',
      'localhost',
    ].join(', '),
    relayhost      => '',
    inet_protocols => 'all',
    smtp_listen    => 'all',
    master_smtps   => [
      'smtps',  # service
      'inet',   # type
      'n',      # private
      '-',      # unpriv
      'n',      # chroot
      '-',      # wakeup
      '-',      # makeproc
      "smtpd ${smtpd_args}"   # command
      ].join("\t"),
  }

  postfix::config {
    'home_mailbox':
      value => 'Maildir/', ;
  }


  ensure_packages([
    'dovecot-imapd',
  ])

  file_line {
    default:
      path   => '/etc/dovecot/conf.d/10-mail.conf',
      notify => Service['dovecot'], ;
    'Dovecot maildir':
      match => '^#?mail_location =',
      line  => 'mail_location = maildir:~/Maildir', ;
    'Dovecot mail access group':
      match => '^#?mail_access_groups =',
      line  => 'mail_access_groups = mail', ;
  }

  file_line {
    default:
      path   => '/etc/dovecot/conf.d/10-ssl.conf',
      notify => Service['dovecot'], ;
    'cert':
      match => '^#?ssl_cert =',
      line  => "ssl_cert = <${cert_path}/fullchain.pem", ;
    'key':
      match => '^#?ssl_key =',
      line  => "ssl_key = <${cert_path}/privkey.pem", ;
  }

  file_line {
    default:
      path   => '/etc/dovecot/conf.d/10-auth.conf',
      notify => Service['dovecot'], ;
    'auth_mechanisms':
      match => '#?auth_mechanisms =',
      line  => 'auth_mechanisms = plain login',
  }
  #
  # TODO
  # i /etc/dovecot/conf.d/10-mail.conf
  # innuti `service auth {` ... `}`
  # lägg till
  $dovecot_auth = @(EOF)
  unix_listener /var/spool/postfix/private/auth {
    mode = 0666
    user = postfix
    group = postfix
  }
  | EOF

  service { 'dovecot':
    ensure => running,
    enable => true,
  }

  postfix::config {
    'smtpd_tls_cert_file':
      value => "${cert_path}/fullchain.pem", ;
    'smtpd_tls_key_file':
      value => "${cert_path}/privkey.pem", ;
    'smtpd_sasl_auth_enable':
      value => 'yes', ;
    'smtpd_sasl_type':
      value => 'dovecot', ;
    'smtpd_sasl_path':
      value => 'private/auth', ;
  }

  ##################################################

  class { 'profiles::wordpress':
    manage_server   => 'nginx',
    manage_database => true,
    nginx_server    => $servername,
    ssl             => if $cert_path { true } else { false },
  }

  ##################################################

  group { ['vastgota', 'styrelse']:
    ensure => present,
  }

  file_line {
  default:
    path => '/etc/default/useradd', ;
  'Default shell':
    match => '^#? *SHELL=',
    line  => 'SHELL=/bin/bash', ;
  'Create mail spool':
    match => '^#? *CREATE_MAIL_SPOOL=',
    line  => 'CREATE_MAIL_SPOOL=yes', ;
  }

  file { '/etc/skel/Maildir':
    ensure => 'directory'
  }

  class { 'profiles::locales':
    locales => [
      [ 'en_US.UTF-8', 'UTF-8' ],
      [ 'sv_SE.UTF-8', 'UTF-8' ],
      [ 'sv_SE', 'ISO-8859-1' ],
    ],
  }

}
