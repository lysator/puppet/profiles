class profiles::epson_scanner {

  # iscan and iscan-data is from epson. http://download.ebz.epson.net/man/linux/iscan_e.html
  # They are however manually downloaded from
  # https://download2.ebz.epson.net/iscan/general/rpm/x64/iscan-bundle-2.30.4.x64.rpm.tar.gz
  # and placed in lysators repo, which is required for this module to
  # work.

  ensure_packages([
    'simple-scan', 'sane-backends',
    'iscan-data', 'iscan',
  ])

  file { '/etc/udev/rules.d/80-epson.rules':
    content => 'SUBSYSTEM=="usb", ENV{ID_MODEL}=="Perfection1200", ACTION=="add", MODE="0666"'
  }
}
