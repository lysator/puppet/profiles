# This ensures that cronmail gets delivered.
# Note that it also requires that hostname is set to our fqdn,
# so /etc/hostname == <machinename>.lysator.liu.se
#
class profiles::cronmailer {
  package { 'msmtp':
    ensure => installed,
  }
  file { '/etc/msmtprc':
    ensure => file,
    source => 'puppet:///modules/profiles/msmtprc',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }
  file_line { 'create root alias':
    ensure => present,
    path   => '/etc/aliases',
    line   => "root: root+${::hostname}@lysator.liu.se",
    match  => '^root:',
  }

  case $facts['os']['family'] {
    'Debian': {
      package { 'msmtp-mta':
        ensure => installed,
      }
    }
    'Suse': {
      package { 'postfix':
        ensure => absent,
      }
      ~> package { 'msmtp-mta':
        ensure => installed,
      }
    }
  }
}
