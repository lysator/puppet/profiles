# Sets up letsencrypt for this host
class profiles::letsencrypt (
  Enum['nginx','apache'] $provider,
  String $certname = $::fqdn,
  Array[String] $domains = [ $::fqdn, ],
) {

  include ::letsencrypt

  # TODO general restart comman
  $plugin = $provider
  $post_hook = $provider ? {
    'nginx'  => 'systemctl restart nginx.service',
    'apache' => 'systemctl restart apache2.service',
  }

  # Hack for certbot to find nginx when run from cron.
  #
  # Under cron, PATH does not contain /usr/sbin by default, causing certbot not
  # to find nginx.
  #
  # The ideal solution would be to set the environment parameter of
  # letsencrypt::certonly. However, that parameter gets used both for the exec
  # resource (when puppet runs certbot directly to initialize it) *and* to
  # configure the environment in the generated shell script called from cron.
  #
  # We do not want to set PATH via the environment parameter since the exec
  # resource in letsencrypt::certonly also sets path, causing warnings:
  #
  #     Warning: Exec[letsencrypt certonly milliways-8.lysator.liu.se](provder=shell): Overriding environment setting 'PATH' with '/usr/sbin:/usr/bin:/bin'
  #
  file { '/usr/bin/nginx':
    ensure => link,
    target => '/usr/sbin/nginx',
  }

  case $facts['os']['family'] {
    'Debian': {
      $nginx_plugin  = 'python3-certbot-nginx'
      $apache_plugin = 'python3-certbot-apache'
    }
    'RedHat': {
      if $facts['os']['name'] == 'Fedora' {
        $nginx_plugin  = 'python3-certbot-nginx'
        $apache_plugin = 'python3-certbot-apache'
      } else {
        case $facts['os']['release']['major'] {
          '7': {
            $nginx_plugin  = 'python2-certbot-nginx'
            $apache_plugin = 'python2-certbot-apache'
          }
          default: {
            $nginx_plugin  = 'python3-certbot-nginx'
            $apache_plugin = 'python3-certbot-apache'
          }
        }
      }
    }
    'Archlinux': {
      $nginx_plugin  = 'certbot-nginx'
      $apache_plugin = 'certbot-apache'
    }
    'FreeBSD': {
      $nginx_plugin  = 'py38-certbot-nginx'
      $apache_plugin = 'py38-certbot-apache'
    }
  }

  case $provider {
    'apache': {
      ensure_packages ([$apache_plugin])
    }
    'nginx': {
      ensure_packages ([$nginx_plugin])
    }
  }

  letsencrypt::certonly { $certname:
    ensure             => present,
    domains            => $domains,
    manage_cron        => true,
    plugin             => $plugin,
    additional_args    => [ '--quiet', ],
    post_hook_commands => [ $post_hook, ],
  }
}
