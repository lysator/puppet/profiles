class profiles::vitellary {

  class { '::profiles::ipa_client':
    manage_home => true,
  }
  include ::profiles::root
#  include ::profiles::ssh_host_keys
  include ::profiles::auto_update

  package {
    [
      'locales-all',
      'pandoc',
      'info',
      'curl',
      'elinks',
      'pass',
      'alpine',
      'alpine-doc',
      'irssi',
      'weechat',
      'sendmail',
      'finger',
    ]:
      ensure => installed,
  }

  motd { 'welcome':
    content => @(EOF)
      #!/bin/sh
      echo Välkommen till Vitellary!
      | EOF
  }
}
