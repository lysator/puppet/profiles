class profiles::zabbix_agent (
  $version = '6.0',
) {
  if $facts['os']['family'] == 'redhat' and $facts['os']['name'] != 'Fedora' {
    package { ['zabbix-release', 'zabbix-agent']:
      ensure => absent,
    }
    file { [
      '/etc/yum.repos.d/zabbix.repo',
      '/etc/yum.repos.d/zabbix-agent2-plugins.repo',
    ]:
      ensure => 'absent',
    }
  } else {
    ensure_packages(['zabbix-agent'], { ensure => absent })
  }
}
