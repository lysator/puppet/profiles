# Configures Wordpress
# Only tested on Debian 11
# TODO ssl for nginx locations
#
# TODO add extlib as dependency/to environment, and use it
class profiles::wordpress (
  Optional[Enum['nginx']] $manage_server = undef,
  Boolean $manage_database = false,
  String $db_name = "wp-${::fqdn}",
  String $db_user = "wp-${::fqdn}",
  # String $db_password = extlib::cache_data("wordpress-${::fqdn}", 'db_password', extlib::random_password()),
  String $db_password = 'changeme',
  String $db_host = 'localhost',
  # String $secret_key = extlib::cache_data("wordpress-${::fqdn}", 'db_password', extlib::random_password()),
  String $secret_key = 'reallychangeme',
  String $wp_content_dir = '/var/lib/wordpress/wp-content',
  Optional[String] $nginx_server = undef,
  Boolean $ssl = false,
) {

  ensure_packages(['wordpress'])

  if ($manage_server == 'nginx') {
    if ($nginx_server == undef) {
      fail('Nginx server must be set to be able to manage nginx server')
    }

    ensure_packages(['php-fpm'])

    # Locations copied from Wordpress's nginx sample config
    nginx::resource::location {
    default:
      server   => $nginx_server,
      ssl      => $ssl,
      ssl_only => $ssl,
      ;
    'WP /':
      location  => '/',
      try_files => [ '$uri', '$uri/', '/index.php?$args' ],
      ;

    'WP ~^/wp-content/(.*)$':
      location       => '~^/wp-content/(.*)$',
      location_alias => "${wp_content_dir}/\$1",
      ;

    'WP ~ \.php$':
      location            => '~ \.php$',
      # Debian alternatives resolves this to the correct version
      fastcgi             => 'unix:/var/run/php/php-fpm.sock',
      fastcgi_index       => 'index.php',
      fastcgi_param       => {
        'SCRIPT_FILENAME' => '$request_filename',
      },
      location_cfg_append => {
        'fastcgi_intercept_errors' => 'on',
      },
      ;
    'WP ~* \.(js|css|png|jpg|jpeg|gif|ico)$':
      location            => '~* \.(js|css|png|jpg|jpeg|gif|ico)$',
      expires             => 'max',
      location_cfg_append => {
        'log_not_found'   => 'off',
      }
    }
  }


  if ($manage_database) {
    include ::mysql::server

    mysql::db { $db_name:
      user     => $db_user,
      password => $db_password,
      host     => $db_host,
      grant    => 'all'
    }
  }

  ##################################################

  file { "/etc/wordpress/config-${::fqdn}.php":
    owner   => 'www-data',
    content => @("EOF")
    <?php
    # Managed by Puppet
    define('DB_NAME', '${db_name}');
    define('DB_USER', '${db_user}');
    define('DB_PASSWORD', '${db_password}');
    define('DB_HOST', '${db_host}');
    define('SECRET_KEY', '${secret_key}');
    define('WP_CONTENT_DIR', '${wp_content_dir}');
    define('DB_CHARSET', 'utf8');
    define('DB_COLLATE', '');
    | EOF
  }

  file { '/var/lib/wordpress/wp-content':
    owner   => 'www-data',
    recurse => true,
  }
}
