# Installs host keys from hiera
class profiles::ssh_host_keys (
  $ssh_ed25519_key = undef,
  $ssh_ed25519_key_pub = undef,
  $ssh_ecdsa_key = undef,
  $ssh_ecdsa_key_pub = undef,
  $ssh_rsa_key = undef,
  $ssh_rsa_key_pub = undef,
  $base_dir = '/etc/ssh/',
  $service = 'sshd',
){
  # Remove DSA-keys if present for some unholy reason
  file { "${base_dir}/ssh_host_dsa_key":
    ensure => absent,
    notify => Service[$service],
  }
  file { "${base_dir}/ssh_host_dsa_key.pub":
    ensure => absent,
    notify => Service[$service],
  }

  # Install public keys
  file {
    default:
      mode   => '0644',
      notify => Service[$service];
    "${base_dir}/ssh_host_rsa_key.pub": content => Sensitive($ssh_rsa_key_pub);
    "${base_dir}/ssh_host_ecdsa_key.pub": content => Sensitive($ssh_ecdsa_key_pub);
    "${base_dir}/ssh_host_ed25519_key.pub": content => Sensitive($ssh_ed25519_key_pub);
  }
  # Install private keys
  file {
    default:
      mode   => '0600',
      notify => Service[$service];
    "${base_dir}/ssh_host_rsa_key": content => Sensitive($ssh_rsa_key);
    "${base_dir}/ssh_host_ecdsa_key": content => Sensitive($ssh_ecdsa_key);
    "${base_dir}/ssh_host_ed25519_key": content => Sensitive($ssh_ed25519_key);
  }
}
