# Base install for most Lysator service machines.
# Usually a good idea to add this.
class profiles::service (
  Boolean $use_legacy_lyslogin = true,  # lyslogin should eventually be fully removed, use a parameter for now to avoid inadvertently breaking other systems
) {
  require ::lysnetwork::public_ip
  include ::packages::base
  # include ::ntp
  include ::lyslogclient
  include ::lysnetwork::ssh
  include ::profiles::monitoring_agents
  include ::profiles::puppet_agent
  if $use_legacy_lyslogin {
    require ::lyslogin
  } else {
    include ::profiles::ipa_client
  }

  # EC2 has something to do with Amazons clouds.
  # By default facter tries to look these up by requesting data from
  # 199.254.169.254, which is an internal address for Amazan.
  # This disables the fact group EC2. Available groups for blocking
  # can be found by running 'facter --list-block-groups'.
  file { '/etc/puppetlabs/facter':
    ensure => directory,
  }
  file { '/etc/puppetlabs/facter/facter.conf':
    ensure  => file,
    content => @(EOF)
    facts : {
      blocklist : [ "EC2" ],
    }
    | EOF
  }

  if ($facts['os']['family'] == 'RedHat') {
    include ::profiles::cronmailer
  }
}
