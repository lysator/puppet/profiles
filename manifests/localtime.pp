# Ensures that the localtime is of this timezone
class profiles::localtime (
  String $timezone,
) {
  file { '/etc/localtime':
    ensure => link,
    target => "/usr/share/zoneinfo/${timezone}",
  }
}
