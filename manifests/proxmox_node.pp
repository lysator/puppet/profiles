class profiles::proxmox_node {
  # Setup login
  # TODO proper pam module. So that user can log in through the web
  # ui, but not ssh in.
  class { '::lyslogin':
    roots_only   => false,
    has_homedirs => false,
  }

  # Setup networking
  $last_octet = split($facts['networking']['interfaces'][$facts['networking']['primary']]['bindings'][0]['address'], '\.')[3]
  $last_octet_16 = String(Integer($last_octet), '%x')
  $ib_iface = 'ibo3'
  $eth_iface = 'eno1'
  $bridge_iface = 'vmbr0'

  include ::lyslagring::hosts

  # resolv.conf is inherited by (at least) LXC-containers created
  # through the puppet admin interface if nothing is set.
  file { '/etc/resolv.conf':
    content => @(EOF)
    search lysator.liu.se
    nameserver 2001:6b0:17:f0a0::e1
    nameserver 130.236.254.225
    nameserver 130.236.254.4
    |- EOF
  }

  # These two lines are needed for NFSv4
  file_line { 'nfs4 statd':
    path  => '/etc/default/nfs-common',
    line  => 'NEED_STATD="no"',
    match => '^NEED_STATD',
  }

  file_line { 'nfs4 idmapd':
    path  => '/etc/default/nfs-common',
    line  => 'NEED_IDMAPD="yes"',
    match => '^NEED_IDMAPD',
  }

  network::interface { "${bridge_iface}-ipv4":
    interface       => $bridge_iface,
    family          => 'inet',
    ipaddress       => "130.236.254.${last_octet}",
    netmask         => '255.255.255.0',
    gateway         => '130.236.254.1',
    bridge_ports    => [ $eth_iface, ],
    bridge_stp      => 'off',
    bridge_fd       => '0',
    dns_nameservers => '130.236.254.225 130.236.254.4',
  }

  network::interface { "${bridge_iface}-ipv6":
    interface       => $bridge_iface,
    family          => 'inet6',
    ipaddress       => "2001:6b0:17:f0a0::${last_octet_16}",
    gateway         => '2001:6b0:17:f0a0::1',
    netmask         => '64',
    bridge_ports    => [ $eth_iface, ],
    bridge_stp      => 'off',
    bridge_fd       => '0',
    dns_nameservers => '2001:6b0:17:f0a0::e1',
  }

  concat::fragment { "interface-${eth_iface}":
    target  => '/etc/network/interfaces',
    content => "iface ${eth_iface} inet manual\n",
    order   => '08',
  }

  ipoib::interface { $ib_iface:
    ipaddress => "10.44.1.${last_octet}",
    netmask   => '255.255.0.0',
  }
  # Install some missing packages
  ensure_packages( ['net-tools'], { ensure => latest })


  # Setup firewall
  include ::lysnetwork::iptables_default_deny
  firewall { '100 allow http(s)':
    dport  => [80, 443],
    proto  => tcp,
    jump   => accept,
  }
  firewall { '200 allow ipoib-traffic':
    iniface => $ib_iface,
    proto   => all,
    jump    => accept,
  }
  firewall { '100 allow http(s) ipv6':
    dport    => [80, 443],
    proto    => tcp,
    jump     => accept,
    protocol => 'ip6tables',
  }
  # Allow zabbix and prometheus agents
  firewall { '101 allow zabbix':
    dport  => 10050,
    proto  => tcp,
    jump   => accept,
    source => '130.236.254.0/24',
  }
  firewall { '102 allow node-exporter':
    dport  => 9100,
    proto  => tcp,
    jump   => accept,
    source => '130.236.254.0/24',
  }
  @@firewall { "203 allow cluster-node ${fqdn}":
    jump   => accept,
    proto  => all,
    source => $facts['networking']['ip'],
    tag    => 'milliways',
  }

  # Temporary solution while the cluster is migrated to new puppet server
  [
    '130.236.254.160',
    '130.236.254.161',
    '130.236.254.162',
    '130.236.254.163',
    '130.236.254.164',
    '130.236.254.166',
    '130.236.254.167',
    '130.236.254.168',
    '130.236.254.169',
  ].each | $millyways_ip | {
    firewall { "203 allow cluster-node ${millyways_ip}":
      jump   => accept,
      proto  => all,
      source => $millyways_ip,
    }
  }

  Firewall <<| tag == 'milliways' |>>

  file { '/etc/systemd/system/nginx.service.d':
    ensure => directory,
  }
  -> file { '/etc/systemd/system/nginx.service.d/override.conf':
    ensure  => file,
    content => @(EOF)
    [Service]
    RuntimeDirectory=nginx
    | EOF
  }

  # Expose UI on a decent port
  include ::nginx
  nginx::resource::server { $facts['networking']['fqdn']:
    server_name               => [$facts['networking']['fqdn']],
    use_default_location      => false,
    # Encrypt everything
    ssl_redirect              => true,
    ssl                       => true,
    ssl_cert                  => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem",
    ssl_key                   => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/privkey.pem",

    # Set the paranoia level to 'high'.
    ssl_protocols             => 'TLSv1.2',
    ssl_ciphers               => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',
    ssl_prefer_server_ciphers => 'on',
  }

  # This shouldn't have to be here, but since the
  # use_default_location => false
  # above apparently doesn't work we force it here.
  file { '/etc/nginx/sites-enabled/default':
    ensure => absent,
  }

  nginx::resource::location { 'proxmox-ui':
    ensure                => present,
    proxy_connect_timeout => '3600s',
    proxy_read_timeout    => '3600s',
    proxy_send_timeout    => '3600s',
    proxy_http_version    => '1.1',
    location              => '/',
    server                => $facts['networking']['fqdn'],
    ssl                   => true,
    ssl_only              => true,
    proxy                 => 'https://127.0.0.1:8006/',
    location_cfg_append   => {
      proxy_buffering      => 'off',
      client_max_body_size => '0',
      send_timeout         => '3600s',
    },
    proxy_set_header      => [
      'Upgrade $http_upgrade',
      'Connection "Upgrade"',
    ],
  }

  class { '::letsencrypt':
    config  => {
      email => 'hx@lysator.liu.se',
    }
  }

  class { '::profiles::letsencrypt':
    certname => $facts['networking']['fqdn'],
    provider => 'nginx',
  }

  apt::source { 'proxmox-community':
    location => 'http://download.proxmox.com/debian/pve',
    repos    => 'pve-no-subscription',
    release  => 'bullseye',
  }

  file_line { 'remove_proxmox_subscription_popup':
    ensure             => present,
    path               => '/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js',
    line               => 'Proxmox.Utils.checked_command = function(checked_command) { checked_command(); } /* disable subscription check */',
    append_on_no_match => true,
  }
  ~> service { 'pveproxy':
  }
}
