class profiles::analysator_login {
  include ::profiles::ipa_client
  class { '::analysator::node': login => true, }
  include ::analysator::desktop
  include ::prometheus::node_exporter
  include ::analysator::packages::build_node
  require ::lysnetwork::iptables
  include ::profiles::zabbix_agent
  include ::profiles::puppet_agent

  service { 'slurmd':
    ensure => stopped,
  }
  $ib_iface = 'ib0'
  $eth_iface = 'eno1'
  $last_octet = split($facts['networking']['interfaces']['eno1']['bindings'][0]['address'], '\.')[3]
  network::interface { $eth_iface:
    ipaddress            => "10.41.0.${last_octet}",
    netmask              => '255.255.255.0',
    options_extra_redhat => {
      'ONBOOT'   => 'yes',
    },
  }

  ipoib::interface { $ib_iface:
    ipaddress => "10.44.4.${last_octet}",
    netmask   => '255.255.0.0',
    gateway   => '10.44.4.1',
    domain    => 'lysator.liu.se',
    dns1      => '130.236.254.4',
    dns2      => '130.236.254.225',
  }
  $public_last_octet = split($facts['networking']['interfaces']['eno2']['bindings'][0]['address'], '\.')[3]

  network::interface { 'eno2':
    ipaddress      => "130.236.254.${public_last_octet}",
    netmask        => '255.255.255.0',
    domain         => 'lysator.liu.se',
    gateway        => '130.236.254.1',
    dns1           => '130.236.254.4',
    dns2           => '130.236.254.225',
    ipv6init       => 'yes',
    ipv6addr       => '2001:6b0:17:f0a0::b5',
    ipv6_defaultgw => '2001:6b0:17:f0a0::1',
  }
}
