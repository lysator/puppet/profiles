# Configures a combined vsm server and vsm agent, orchestrated through
# thinlinc. Requires that thinlinc is manually installed beforehand.
class profiles::thinlinc (
  String $agent_hostname = 'thinlinc.lysator.liu.se',
) {


  # Configure systemd unit
  # ideally a 'systemctl daemon-reload' should be executed after these
  # changes.
  file { '/etc/systemd/system/thinlinc.target':
    content => @(EOF)
    [Unit]
    Description=Combined Thinlinc services
    | EOF
  }

  file { [
    '/etc/systemd/system/vsmagent.service.d',
    '/etc/systemd/system/vsmserver.service.d',
  ]:
    ensure => 'directory',
  }

  file { [
    '/etc/systemd/system/vsmagent.service.d/override.conf',
    '/etc/systemd/system/vsmserver.service.d/override.conf',
  ]:
    content => @(EOF)
    [Unit]
    PartOf=thinlinc.target
    | EOF
  }

  # The settings "/{tlwebadm,tlwebccess}/{cert,certkey}" exists, but
  # changing them seems to do nothing. Instead we place our good
  # certificate where thinlinc expects them

  # Configure certs for web access
  file { '/opt/thinlinc/etc/tlwebaccess/server.crt':
    ensure => link,
    target => '/etc/letsencrypt/live/thinlinc.lysator.liu.se/cert.pem',
    backup => '.local',
  }

  file { '/opt/thinlinc/etc/tlwebaccess/server.key':
    ensure => link,
    target => '/etc/letsencrypt/live/thinlinc.lysator.liu.se/privkey.pem',
    backup => '.local',
  }

  # Configure certs for web admin
  file { '/opt/thinlinc/etc/tlwebadm/server.crt':
    ensure => link,
    target => '/etc/letsencrypt/live/thinlinc.lysator.liu.se/cert.pem',
    backup => '.local',
  }

  file { '/opt/thinlinc/etc/tlwebadm/server.key':
    ensure => link,
    target => '/etc/letsencrypt/live/thinlinc.lysator.liu.se/privkey.pem',
    backup => '.local',
  }


  ini_setting { 'Set thinlinc /vsmagent/agent_hostname':
    ensure  => present,
    path    => '/opt/thinlinc/etc/conf.d/vsmagent.hconf',
    section => '/vsmagent',
    setting => 'agent_hostname',
    value   => $agent_hostname,
  }

}
