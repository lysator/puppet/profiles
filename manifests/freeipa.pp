class profiles::freeipa {
  ensure_packages(['ipa-server'], { ensure => latest, })
  # Allow http, to allow https redirect to work
  firewall { '922 allow http':
    proto  => tcp,
    dport  => '80',
    source => '130.236.254.0/24',
    jump   => 'accept',
  }

  firewall { '922 allow http ipv6':
    proto    => tcp,
    dport    => '80',
    source   => '2001:6b0:17:f0a0::/64',
    jump     => 'accept',
    protocol => 'ip6tables',
  }


  # Allow https
  firewall { '922 allow https':
    proto  => tcp,
    dport  => '443',
    source => '130.236.254.0/24',
    jump   => 'accept',
  }

  firewall { '922 allow https ipv6':
    proto    => tcp,
    dport    => '443',
    source   => '2001:6b0:17:f0a0::/64',
    jump     => 'accept',
    protocol => 'ip6tables',
  }

  # Allow DNS
  firewall { '922 allow DNS':
    proto  => tcp,
    dport  => '53',
    jump   => 'accept',
  }
  firewall { '922 allow DNS ipv6':
    proto    => tcp,
    dport    => '53',
    jump     => 'accept',
    protocol => 'ip6tables',
  }
  firewall { '922 allow DNS udp':
    proto  => udp,
    dport  => '53',
    jump   => 'accept',
  }
  firewall { '922 allow DNS udp ipv6':
    proto    => udp,
    dport    => '53',
    jump     => 'accept',
    protocol => 'ip6tables',
  }

  # Allow LDAP
  firewall { '922 allow LDAP':
    proto  => tcp,
    dport  => '389',
    jump   => 'accept',
    source => '130.236.254.0/24',
  }
  firewall { '922 allow LDAP ipv6':
    proto    => tcp,
    dport    => '389',
    jump     => 'accept',
    protocol => 'ip6tables',
    source => '2001:6b0:17:f0a0::/64',
  }
  # Allow LDAPS
  firewall { '922 allow LDAPS':
    proto  => tcp,
    dport  => '636',
    jump   => 'accept',
    source => '130.236.254.0/24',
  }
  firewall { '922 allow LDAPS ipv6':
    proto    => tcp,
    dport    => '636',
    jump     => 'accept',
    protocol => 'ip6tables',
    source => '2001:6b0:17:f0a0::/64',
  }
  # Allow Kerberos
  firewall { '922 allow Kerberos':
    proto  => udp,
    dport  => '88',
    jump   => 'accept',
    source => '130.236.254.0/24',
  }
  firewall { '922 allow Kerberos ipv6':
    proto    => udp,
    dport    => '88',
    jump     => 'accept',
    protocol => 'ip6tables',
    source => '2001:6b0:17:f0a0::/64',
  }
  firewall { '922 allow Kerberos tcp':
    proto  => tcp,
    dport  => '88',
    jump   => 'accept',
    source => '130.236.254.0/24',
  }
  firewall { '922 allow Kerberos tcp ipv6':
    proto    => tcp,
    dport    => '88',
    jump     => 'accept',
    protocol => 'ip6tables',
    source => '2001:6b0:17:f0a0::/64',
  }

  # Allow NTP
  firewall { '922 allow NTP':
    proto  => udp,
    dport  => '123',
    jump   => 'accept',
    source => '130.236.254.0/24',
  }
  firewall { '922 allow NTP ipv6':
    proto    => udp,
    dport    => '123',
    jump     => 'accept',
    protocol => 'ip6tables',
    source => '2001:6b0:17:f0a0::/64',
  }

  # Allow FreeIPA client
  ['2001:6b0:17:f0a0::/64', '130.236.254.0/24'].each |$source| {
    ['tcp', 'udp'].each |$proto| {
      case ':' in $source {
        true: {
          $ipv = '6'
          $provider = 'ip6tables'
        }
        default: {
          $ipv = '4'
          $provider = 'iptables'
        }
      }
      firewall { "922 allow FreeIPA client IPv${ipv}/${proto}":
        proto    => $proto,
        dport    => '464',
        jump     => 'accept',
        protocol => $provider,
        source   => $source,
      }
    }
  }

  # Make NetworkManager stop fucking up resolv.conf
  $nm_dns_none = @(EOT)
    [main]
    dns=none
    | EOT
  file { '/etc/NetworkManager/conf.d/90-dns-none.conf':
    ensure  => file,
    content => $nm_dns_none,
  }

  # And add one of our own making
  $resolv_conf = @(EOT)
    search ad.lysator.liu.se
    nameserver 2001:6b0:17:f0a0::e1
    nameserver 130.236.254.225
    nameserver 130.236.254.4
    | EOT
  file { '/etc/resolv.conf':
    ensure  => file,
    content => $resolv_conf,
  }
}
