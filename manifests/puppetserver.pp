class profiles::puppetserver {

  class { '::puppet':
    server                                 => true,
    server_reports                         => 'puppetdb,foreman',
    server_environment_class_cache_enabled => true,
    autosign_entries                       => [
      '*.lysator.liu.se',
    ],
    server_storeconfigs                    => true,
    # prevent creation of an unused 'common' environment
    # (the default server_common_modules_path also contains some other paths, but we don't use any of them)
    server_common_modules_path             => '',
  }

  class { '::foreman':
    db_manage      => false,
  }

  class { '::foreman::repo':
    repo => '3.13',
  }

  class { '::postgresql::globals':
    version => '12.9',
  }

  class { '::foreman_proxy':
    puppet   => true,
    puppetca => true,
    tftp     => false,
    dhcp     => false,
    dns      => false,
    bmc      => false,
    realm    => false,
  }

  # Setup r10k
  class { 'r10k':
    remote => 'https://git.lysator.liu.se/lysator/puppet/r10k.git',
    git_settings => {
      default_ref => 'master',
    },
  }
  # Which needs git to work
  ensure_packages( [
    'git',
    # 'rubygem-hiera-eyaml',
  ])


  # Update the environments once in a while...
  cron { 'r10k update':
    command => '/usr/bin/r10k deploy environment --puppetfile',
    user    => 'root',
    minute  => '*/2',
  }

  $hiera_conf = {
    version =>  5,
    defaults =>  {
      'datadir'   => '/etc/puppetlabs/code/environments/%{::environment}/data',
      'lookup_key' => 'eyaml_lookup_key',
      'options' => {
        'pkcs7_private_key' => '/etc/puppetlabs/puppet/keys/private_key.pkcs7.pem',
        'pkcs7_public_key'  => '/etc/puppetlabs/puppet/keys/public_key.pkcs7.pem'
      },
    },
    hierarchy =>  [
      {
        'name' =>  'Default lookups',
        'paths' =>  [
          'default/nodes/%{::trusted.certname}.yaml',
          'default/os/%{facts.os.name}/%{facts.os.release.major}.yaml',
          'default/os/%{facts.os.family}/%{facts.os.release.major}.yaml',
          'default/os/%{facts.os.family}/%{facts.kernelrelease}.yaml',
          'default/os/%{facts.os.name}.yaml',
          'default/os/%{facts.os.family}.yaml',
          'default/common.yaml',
        ],
      },
    ],
  }

  file { '/etc/puppetlabs/puppet/hiera.yaml':
    ensure  => file,
    content => hash2yaml($hiera_conf),
    owner   => 'puppet',
    group   => 'puppet',
    mode    => '0644',
  }
  Service <| title == 'puppetserver' |> {
    subscribe +> File['/etc/puppetlabs/puppet/hiera.yaml'],
  }

  # Dependency required by the grafana module
  # (installed for both the puppetserver and for the agent/r10k)
  # https://github.com/voxpupuli/puppet-grafana?tab=readme-ov-file#toml-note
  ['puppetserver_gem', 'puppet_gem'].each |$provider| {
    package { "${provider}-toml":
      ensure   => installed,
      name     => 'toml',
      provider => $provider,
    }
  }

  # Only allow puppetdb access from Lysator's network.
  firewall { '900 puppetdb block':
    proto  => tcp,
    dport  => '8080',
    source => '! 130.236.254.0/24',
    jump   => 'drop',
  }
  firewall { '900 puppetdb block ipv6':
    proto    => tcp,
    dport    => '8080',
    source   => '! 2001:6b0:17:f0a0::/64',
    jump     => 'drop',
    protocol => 'ip6tables',
  }
}
