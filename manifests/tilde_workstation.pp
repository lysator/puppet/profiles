class profiles::tilde_workstation {
  include ::workstation
  class { '::profiles::ipa_client':
    manage_home => true,
  }
  include ::profiles::root
  include ::profiles::ssh_host_keys
  include ::profiles::gdm
  include ::profiles::auto_update
}
