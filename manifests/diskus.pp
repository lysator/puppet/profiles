class profiles::diskus {
  require ::firewall
  require ::lysnetwork::fail2ban
  require ::lysnetwork::iptables_only_lysator
  require ::profiles::cronmailer
  require ::profiles::ipa_client
  require ::profiles::pam_only_roots
}
