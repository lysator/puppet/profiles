class profiles::lysator_repo (
  Boolean $deb822 = false,
){
  case $facts['os']['family'] {
    'Debian': {
      $osname = downcase($facts['os']['name'])
      $url = 'http://repomaster.lysator.liu.se'
      $codename = $facts['os']['distro']['codename']

      $deb822str = @("EOF")
          Types: deb
          URIs: ${url}/${osname}
          Suites: ${codename}
          Components: main
          | EOF

      $debstr = "deb ${url}/${osname} ${codename} main"

      file { '/etc/apt/trusted.gpg.d/lysator.gpg':
        source => 'puppet:///modules/profiles/lysator-repo-key.gpg',
      }

      file { '/etc/apt/sources.list.d/lysator.list':
        content => if $deb822 { $deb822str } else { $debstr },
      }
    }
    'RedHat': {
      $os_url_component = fact('os.name') ? {
        'Fedora' => 'fedora',
        default  => 'el',  # RHEL, CentOS, Rocky, Alma, etc.
      }
      file { '/etc/yum.repos.d/lysator.repo':
        content => @("EOF"/$)
        [lysator]
        name=Lysator
        baseurl=http://repomaster.lysator.liu.se/${os_url_component}/\$releasever/\$basearch
        enabled=1
        gpgcheck=0
        protect=1
        | EOF
      }
    }
    default: {
      fail ('OS Not supported')
    }
  }
}
