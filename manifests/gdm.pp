class profiles::gdm {
  file { '/etc/dconf/profile/gdm':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profiles/dconf_profile_gdm',
    notify => Exec['gdm-dconf-update'],
  }
  file { '/etc/dconf/db/gdm.d':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    notify => Exec['gdm-dconf-update'],
  }
  -> file { '/etc/dconf/db/gdm.d/00-lysator':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profiles/dconf_db_gdm.d_00-lysator',
    notify => Exec['gdm-dconf-update'],
  }

  exec { 'gdm-dconf-update':
    command     => '/usr/bin/dconf update',
    refreshonly => true,
  }
}
