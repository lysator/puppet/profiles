class profiles::auto_update {
  $minute = fqdn_rand(59)
  case $facts['os']['family'] {
    'Debian': {
      ensure_packages(
        [ 'cronic', ],
        { ensure => installed, })
      cron { 'upgrade':
        ensure      => present,
        environment => ['PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin', 'DEBIAN_FRONTEND=noninteractive'],
        command     => 'cronic apt-get -y update && cronic apt-get -y upgrade && cronic apt-get -y autoremove',
        user        => 'root',
        hour        => 3,
        minute      => $minute,
        weekday     => ['1-5'],
      }
      cron { 'reconfig':
        ensure      => absent,
        environment => 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
        command     => 'dpkg --configure -a',
        user        => 'root',
        hour        => '*',
        minute      => '01',
        weekday     => ['1-5'],
      }
    }
    'RedHat': {
      if fact('os.name') == 'CentOS' and versioncmp(fact('os.release.major'), '7') <= 0 {
        ensure_packages(['yum-cron'])
        augeas { 'configure yum-cron':
          lens    => 'Yum.lns',
          incl    => '/etc/yum/yum-cron.conf',
          changes => [
            'set commands/apply_updates yes',
            'set commands/random_sleep 30',  # minutes
            'set emitters/emit_via None',  # prevent anacron mails (default value is stdio)
            # automatically accept new signing keys (without this, all updates will be blocked if a repo replaces its key):
            'set base/assumeyes True',
          ],
          require => Package['yum-cron'],
        }
        # yum-cron is not a persistent service, so no need for refresh here (it
        # just creates a file signaling to a cron job that yum-cron is enabled)
        -> service { 'yum-cron':
          ensure => running,
          enable => true,
        }

        # remove the old cron job
        cron { 'upgrade':
          ensure      => absent,
        }
      } else {
        cron { 'upgrade':
          ensure => absent,
        }

        package { 'dnf-automatic': }
        -> service { 'dnf-automatic-install.timer':
          ensure => running,
          enable => true,
        }
      }
    }
  }
}
