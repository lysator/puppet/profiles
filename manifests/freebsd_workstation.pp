# Profile defining a FreeBSD workstation.
class profiles::freebsd_workstation {
  include ::freebsd::blacklistd
  include ::freebsd::firewall::open
  include ::freebsd::ldap
  include ::freebsd::ldap::user
  include ::freebsd::nfsclient
  include ::freebsd::workstation
  # Shipon is only a remote server and does not need slim.
  if fact('networking.fqdn') != 'shipon.lysator.liu.se' {
	  include ::freebsd::workstation::slim
  }
  include ::profiles::root
}
