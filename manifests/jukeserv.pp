class profiles::jukeserv (
  String $music_path,
) {

  file { '/etc/exports':
    ensure => present,
  } -> file_line { 'jukebox export':
    ensure => present,
    path   => '/etc/exports',
    line   => "${music_path} @lysnet(rw,sync,no_subtree_check)",
  } ~> service { 'nfs':
    ensure => 'running',
    enable => true,
  }

  class { 'mpd::server':
    follow_outside_symlinks => false,
    music_directory         => $music_path,
    mpd_home                => '/var/lib/mpd',
    max_output_buffer_size  => 327680
  }

  mpd::output { 'No Output':
    type   => 'null',
  }

  firewall { '100 Allow MPD Lysator':
    proto  => 'tcp',
    jump   => 'accept',
    dport  => '6600', # mpd
    source => "${facts['network']}/${facts['netmask']}",
  }

  firewall { '100 Allow MPD Lysator IPv6':
    proto    => 'tcp',
    jump     => 'accept',
    dport    => '6600', # mpd
    source   => "${facts['network6']}/${facts['netmask6']}",
    protocol => 'ip6tables',
  }

  # Allow NFS through firewall
  ['tcp', 'udp'].each |$proto| {
    firewall {
      default:
        proto  => $proto,
        dport  => [111, 20048, 2049], # 'portmapper', 'mountd', 'nfs'
        jump   => 'accept', ;
      "050 Accept mounts ${proto} from Lysator":
        source => '130.236.254.0/24', ;
      "051 Accept mounts ${proto} from Lysator":
        source => '130.236.253.0/24', ;
      "050 Accept mounts ${proto} from Lysator IPv6":
        source   => '2001:6b0:17:f0a0::0/64',
        protocol => 'ip6tables', ;
    }
  }
}
