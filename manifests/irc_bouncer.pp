class profiles::irc_bouncer (
) {
  include ::znc
  include ::znc::sasl
  include ::znc::ident

  require ::profiles::lysator_repo

  ensure_packages(['znc-lysconf'])
  znc::module { 'lysconf': }

  file { '/var/lib/znc/moddata/cyrusauth/.registry':
    ensure  => file,
    content => "CreateUser true\n",
  }

  # Letsencrypt challenge is done through DNS since the ZNC webserver
  # is to inflexible, and I neither want to front it just for certs,
  # and I don't want to stop ZNC just for updating certs.

  # https://certbot-dns-rfc2136.readthedocs.io/en/stable/

  # Secret is manually generated on the nameserver, and placed in the
  # appropriate dns config file.
  # $ tsig-keygen -a hmac-sha256 verdigris.lysator.liu.se

  class { '::letsencrypt':
    config  => {
      email => 'hugo@lysator.liu.se',
    }
  }

  $key_name = $facts['fqdn']

  # TODO dynamic dns update fails since the DNS server manually writes
  # the zone file, disregarding the journal. Update Lysators DNS
  # server to not do that.
  class { '::letsencrypt::plugin::dns_rfc2136':
    # address of ns-master.lysator.liu.se
    # Since the python can't handle DNS...
    server              => '2001:6b0:17:f0a0::2',
    key_name            => $key_name,
    key_algorithm       => 'HMAC-SHA256',
    # TODO don't publish secret here
    key_secret          => 'YHR7/5gOkdPF64GwWRu6Ge8jcjz8siqCWIy/G8FsVzw=',
    propagation_seconds => 10,
    manage_package      => true,
  }
  # TODO znc probably needs to be reloaded after cert upgrade
  -> letsencrypt::certonly { $key_name:
    ensure          => 'present',
    domains         => [ $facts['fqdn'], ],
    plugin          => 'dns-rfc2136',
    manage_cron     => true,
    additional_args => [ '--quiet', ],
  }

  # TODO files in archive is still not readable by znc
  file { ['/etc/letsencrypt/live',
          '/etc/letsencrypt/archive', ]:
    ensure => directory,
    mode   => '0750',
    group  => 'znc',
  }

  file { '/var/lib/znc/privkey.pem':
    ensure => link,
    target => "/etc/letsencrypt/archive/${key_name}/privkey1.pem",
  }

  file { '/var/lib/znc/fullchain.pem':
    ensure => link,
    target => "/etc/letsencrypt/archive/${key_name}/fullchain1.pem",
  }
}
