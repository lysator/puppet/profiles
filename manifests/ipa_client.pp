# Join a computer to FreeIPA
class profiles::ipa_client (
  Boolean $manage_home = false,
) {
  class { '::ipa_client':
    domain        => 'ad.lysator.liu.se',
    servers       => [
      'trocca.ad.lysator.liu.se',
      'champis.ad.lysator.liu.se',
    ],
    manage_autofs => $manage_home,
    use_autofs    => $manage_home,
  }
}
