class profiles::milou {
  class { '::profiles::ipa_client':
    manage_home => true,
  }
  include ::debian::server
  include ::lyslogclient
  include ::lysnetwork::fail2ban
  include ::lysnetwork::hosts

  file { '/lysator':
      ensure => 'link',
      target => '/mp/lysator',
    }
    file { '/var/mail':
      ensure => 'link',
      target => '/mp/mail',
      force  => true,
    }
}
