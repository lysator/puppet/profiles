class profiles::puppet_agent {
  if ($::puppetmaster) {
    if fact('os.name') == 'Debian' and versioncmp(fact('os.release.major'), '13') >= 0 {
      # Use the puppet packages packaged by debian (puppetlabs are bad at
      # releasing packages for new distributions in a timely manner)
      file { '/etc/apt/sources.list.d/pc_repo.list':
        ensure => absent,
      }
      ensure_packages([
        'puppet-agent',
        'puppet-module-puppetlabs-augeas-core',
        'puppet-module-puppetlabs-cron-core',
        'puppet-module-puppetlabs-host-core',
        'puppet-module-puppetlabs-mailalias-core',
        'puppet-module-puppetlabs-mount-core',
        'puppet-module-puppetlabs-selinux-core',
        'puppet-module-puppetlabs-sshkeys-core',
      ])
      # Debian's packaging of modules is only useful on a puppet master
      #
      # That's because the packaged modules are installed into the
      # $basemodulepath (/usr/share/puppet/modules on Debian), but modules are
      # only synced to the client from the server's basemodulepath.
      #
      # So if your server runs another OS, and the puppetlabs AIO packages
      # (which includes the core modules), then the server's basemodulepath is
      # empty.
      #
      # For the moduels to be useful they'd have to be installed into the
      # vendormoduledir, as modules in that path are picked up locally (that's
      # also where the core packages are kept in the AIO packages) .
      #
      # Unfortunately, the following code is not all that useful: if the
      # symlinks are already in place, then puppet already works, but if
      # they're not, then puppet does not work (if the node uses manifests that
      # uses functionality from the affected modules).
      #
      # Here's two one-liners that both achieve what is needed (take your pick):
      # apt-get install -y 'puppet-module-puppetlabs-*-core' && for f in /usr/share/puppet/modules/*; do ln -s "../modules/${f##*/}" "/usr/share/puppet/vendor_modules/${f##*/}"; done
      # apt-get install -y 'puppet-module-puppetlabs-*-core' && puppet agent -t --modulepath=/usr/share/puppet/modules
      [
        'augeas_core',
        'cron_core',
        'host_core',
        'mailalias',
        'mount_core',
        'selinux_core',
        'sshkeys_core',
      ].each |$_mod| {
        file { "/usr/share/puppet/vendor_modules/${$_mod}":
          ensure => link,
          target => "../modules/${_mod}",
        }
      }
      ini_setting { 'puppet-server-url':
        ensure  => present,
        path    => '/etc/puppet/puppet.conf',
        section => 'main',
        setting => 'server',
        value   => $::puppetmaster,
        notify  => Service['puppet'],
      }
      ~> service { 'puppet': }
    } else {
      class { 'puppet_agent':
        collection      => 'puppet7',
        manage_repo     => true,
        package_version => 'latest',
      }

      if ($facts['os']['family'] == 'Debian') {
        # apparently needed?
        file { '/etc/apt/trusted.gpg.d/pc_repo_puppet.gpg':
          source         => 'https://apt.puppet.com/keyring.gpg',
          checksum       => 'md5',
          checksum_value => '0f97ab5f9b62f0f041a40db0d1483a73',
        }
      }

      ini_setting { 'puppet-server-url':
        ensure  => present,
        path    => '/etc/puppetlabs/puppet/puppet.conf',
        section => 'main',
        setting => 'server',
        value   => $::puppetmaster,
        notify  => Service['puppet'],
      }
    }
  } else {
    $file = $facts['os']['family'] ? {
      'Debian' => '/etc/puppet/puppet.conf',
      'RedHat' => '/etc/puppetlabs/puppet/puppet.conf',
      default  => fail('This operating system is not supported.')
    }
    file { $file:
      ensure  => file,
      content => "[main]\nserver = ${server}\n",
      notify  => Service['puppet'],
    }
    service { 'puppet':
      ensure => running,
      enable => true,
    }
  }
}
