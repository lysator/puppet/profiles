class profiles::root (
  $password = undef
)
{
  user { 'root':
    name           => 'root',
    gid            => 0,
    uid            => 0,
    home           => '/root',
    managehome     => false,
    password       => Sensitive(chomp($password)),
    purge_ssh_keys => true,
  }
}
