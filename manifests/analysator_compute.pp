class profiles::analysator_compute {
  include ::profiles::ipa_client
  class { '::analysator::node::network': }
  -> class { '::analysator::node': }
  include analysator::packages::build_node
}
