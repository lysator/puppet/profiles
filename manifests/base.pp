# Should be applied on *all* hosts at lysator
class profiles::base {
  case $facts['os']['name'] {
    'Debian': {
      include ::profiles::base::debian
    }
  }
}
