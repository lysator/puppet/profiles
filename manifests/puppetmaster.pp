class profiles::puppetmaster {
  cron { 'Update puppet code':
    command => 'cd /etc/puppetlabs/code/environments/production; /usr/bin/git pull; /usr/bin/git submodule update --init',
  }

  class { 'puppetdb':
    manage_package_repo => false,
    postgres_version    => '11',

  }

  class { 'puppetdb::master::config': }
}
