class profiles::monitoring_agents {
  file { '/var/lib/prometheus-dropzone':
    ensure => directory,
  }

  # Hack for Fedora 35, where the service_provider fact isn't present
  $init_style = (fact('os.name') == 'Fedora' and fact('os.release.major') == '35') ? {
    true    => 'systemd',
    default => undef,
  }

  class { '::prometheus::node_exporter':
    extra_options => '--collector.textfile.directory=/var/lib/prometheus-dropzone',
    init_style    => $init_style,
    require       => File['/var/lib/prometheus-dropzone'],
  }

  firewall { '100 allow node_exporter':
    proto    => tcp,
    dport    => '9100',
    jump   => 'accept',
    protocol => 'iptables',
    source   => '130.236.254.0/24',
  }
  firewall { '100 allow node_exporter ipv6':
    proto    => tcp,
    dport    => '9100',
    jump     => 'accept',
    protocol => 'ip6tables',
    source   => '2001:6b0:17:f0a0::/64',
  }
}
