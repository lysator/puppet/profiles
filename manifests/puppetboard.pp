class profiles::puppetboard {
  # Git managed explicitly instead of through puppetboards manage_git
  # property, since that collides with our ensure_package(git) in
  # packages/base/common.pp.
  ensure_packages(['git'], { ensure => installed })

  class { '::puppetboard':
    manage_virtualenv => true,
    revision          => 'v4.0.5',
    puppetdb_host     => 'chapman',
    puppetdb_port     => 8080,
    require           => [
      Package['git'],
    ],
  }

  # Ensure that apt installs work
  exec { 'apt update':
    command => '/usr/bin/apt update',
  }
  Exec['apt update'] -> Package <| |>

  firewall {
  default:
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => ['80', '443'],
    proto  => 'tcp',
    ;
  '100 www IPv4':
    protocol => 'iptables',
    source   => '130.236.254.0/24',
    ;
  '100 www IPv6':
    protocol => 'ip6tables',
    source   => '2001:6b0:17:f0a0::/64',
    ;
  }


  class { 'apache': }
  class { 'apache::mod::wsgi':
    package_name => 'libapache2-mod-wsgi-py3',
    # Will be a symlynk to mod_wsgi.so-3.7
    mod_path     => 'mod_wsgi.so',
  }

  class { '::profiles::letsencrypt':
    provider => apache,
  }

  # Only set up TLS if we are ready. This allows us to bootstrap
  # ourselves the next run.
  $certname = $::fqdn
  if $certname and $facts['letsencrypt_directory'][$certname] {
    class { 'puppetboard::apache::vhost':
      vhost_name => $::fqdn,
      port       => 443,
      ssl        => true,
      ssl_cert   => "/etc/letsencrypt/live/${certname}/cert.pem",
      ssl_key    => "/etc/letsencrypt/live/${certname}/privkey.pem",
      ssl_chain  => "/etc/letsencrypt/live/${certname}/fullchain.pem",
    }

    apache::vhost { "http-redirect":
      servername      => $::fqdn,
      port            => 80,
      redirect_source => ['/'],
      redirect_dest   => ["https://${::fqdn}/"],
      redirect_status => ['permanent'],
      docroot         => false,
    }
  } else {
    class { 'puppetboard::apache::vhost':
      vhost_name => $::fqdn,
      port       => 80,
      ssl        => false,
    }
  }

  # puppetboard uses Python::Venv, which uses `venv`. However,
  # Apache::Vhost places a wsgi.py, which tries to load
  # activate_this.py, which only `virtualenv` provides.
  # Placing it manually however works.
  file { "${puppetboard::basename}/virtenv-puppetboard/bin/activate_this.py":
    source => "puppet:///modules/${module_name}/activate_this.py",
  }
}
