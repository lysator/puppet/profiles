class profiles::elvisp {
  ensure_packages(
    [
      'lyskom-elisp-client',
    ],
    { ensure => installed })

  file { '/etc/emacs/site-start.d/10lyskom.el':
    mode   => '0444',
    source => 'puppet:///modules/profiles/lyskom-serverlist.el',
  }
}

