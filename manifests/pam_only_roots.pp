class profiles::pam_only_roots {

    $conf = @("EOT")
    +:root:ALL
    +:@root:ALL
    +:root:cron crond
    -:ALL:ALL
    | EOT

    file { '/etc/security/access.conf':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => $conf
    }

    file_line { '/etc/pam.d/sshd pam_access.so':
        path  => '/etc/pam.d/sshd',
        after => '^auth\s+include\s+postlogin$',
        line  => 'account    required     pam_access.so',
    }
}
