#!/bin/bash

CHANNEL=1

if dmidecode -t 2 | grep -q "DL170h G6"; then
        CHANNEL=2
fi


ipmitool lan set $CHANNEL ipsrc static
ipmitool lan set $CHANNEL ipaddr 172.16.254.$(facter networking.ip | cut -d'.' -f4)
ipmitool lan set $CHANNEL netmask 255.255.0.0
ipmitool lan set $CHANNEL defgw ipaddr 172.16.0.1
ipmitool lan set $CHANNEL arp respond on
ipmitool lan set $CHANNEL access on

ipmitool lan set $CHANNEL auth ADMIN MD5 PASSWORD
ipmitool user set password 2 hunter2 # super safe.
ipmitool user set name 2 ADMIN

ipmitool channel setaccess $CHANNEL 2 link=on ipmi=on callin=on privilege=4
